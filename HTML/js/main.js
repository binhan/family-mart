(function ($) {

    $.fn.getTopInPercent = function () {
        var top = parseFloat($(this).css('top'))/parseFloat($(this).parent().parent().css('height'));
        return Math.round(100*top)+'%';
    };

})(jQuery);

var puropela = {
	status: { isReset: true },
	option: { khaiTruongNew: '35%', haiLongNew: '90%', chiaSeNew: '60%', ballNew: '70%', khaiTruongTop: '', haiLongTop: '', chiaSeTop: '', ball1Top: '', ball2Top: '' },

	savePosition: function(){
		this.option.khaiTruongTop = $('.khai-truong').getTopInPercent();
		this.option.haiLongTop = $('.hai-long').getTopInPercent();
		this.option.chiaSeTop = $('.chia-se').getTopInPercent();
		
		this.option.ball1Top = $('.ball1').getTopInPercent();
		this.option.ball2Top = $('.ball2').getTopInPercent();
	},
	resetPosition: function(){
		this.savePosition();

		$('.khai-truong').css({
			'top': this.option.khaiTruongNew,
			'opacity': 0
		});

		$('.hai-long').css({
			'top': this.option.haiLongNew,
			'opacity': 0
		});

		$('.chia-se').css({
			'top': this.option.chiaSeNew,
			'opacity': 0
		});

		$('.ball1').css({
			'top': this.option.haiLongNew,
			'opacity': 0
		});

		$('.ball2').css({
			'top': this.option.haiLongNew,
			'opacity': 0
		});
	},

	endPosition: function(){
		
		$('.ball2').animate({
			top: puropela.option.ball2Top,
			opacity: 1
		}, 600, "easeInOutQuad", function(){
			$('.khai-truong').animate({
				opacity: 1,
				top: puropela.option.khaiTruongTop
			});
		});

		$('.ball1').animate({
			top: puropela.option.ball1Top,
			opacity: 1
		}, 800, "easeInOutQuad", function(){
			$('.chia-se').animate({
				opacity: 1,
				top: puropela.option.chiaSeTop
			}, 500, false);

			$('.hai-long').animate({
				opacity: 1,
				top: puropela.option.haiLongTop
			}, 600, false)
		});

	},

	boundEffect: function(){
		$('.khai-truong').transition( { y: "+=10"}, 1200 ).transition( {y: "-=10"}, 1200 );
	}
}

jQuery(document).ready(function($) {
	
	puropela.resetPosition();
	setTimeout(function(){
		puropela.endPosition();
	},500);
});